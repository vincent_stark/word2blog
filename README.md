word2blog
=========

word2blog converts Word 2007-10 HTML Document to Atom XML file.

This file can be imported to any platform which supports Atom XML, such as [Blogger](http://blogger.com), [WordPress](http://wordpress.org) or [Penzu](http://penzu.com).

Details
-------

Special Word document markup is assumed:
* Entry date should be *Heading 1*-styled
* Entry date should be in *DD.MM.YYYY* format

![Word Document](http://www.vasil-y.com/wp-content/uploads/2012/10/word2blog.png)

Contacts
========
http://vasil-y.com

https://github.com/vasily-ponomarev
